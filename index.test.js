// Core requirements:-
// A production - grade library or framework: - Restify
// Either GraphQL ​or​ REST: REST

const Frisbee = require("frisbee");
const { cartStorage, resetProducts } = require("./store");

let api;
beforeAll(() => {
  api = new Frisbee({
    baseURI: "http://localhost:8080",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    }
  });
});

beforeEach(async () => {
  await cartStorage.clear();
  await resetProducts();
});

test("Products should be listed in this format: product name, price and link to add product", async () => {
  const expectedProductShape = [
    "name",
    "price",
    "quantityAvailable",
    "addToCartLink"
  ];
  const { status, body } = await api.get("/products");
  expect(status).toBe(200);
  expect(Object.keys(body[expectedProductShape.length - 1])).toEqual(
    expectedProductShape
  );
});

test("Must be able to add a product to the cart", async () => {
  const { body: productBody } = await api.get("/products");
  const chosenProduct = productBody[0];
  const { status, ...rest } = await api.post(chosenProduct.addToCartLink);
  expect(status).toBe(200);
});

test("Must be able to view current products in the cart", async () => {
  await api.post("cart/hacksaw");
  const { status, body } = await api.get("/cart");
  expect(status).toBe(200);
  expect(body).toEqual([
    {
      name: "Hacksaw",
      price: "19.45",
      quantity: 1,
      total: "19.45"
    }
  ]);
});

test("Cart products should be listed in this format: product name, price, quantity, total", async () => {
  await api.post("cart/hacksaw");
  const expectedProductShape = ["name", "price", "quantity", "total"];
  const { status, body } = await api.get("/cart");
  expect(Object.keys(body[0])).toEqual(expectedProductShape);
});

test("Cart products should only be added if product is available", async () => {
  const { status } = await api.post("cart/magicitem");
  expect(status).toBe(404);
});

test("Cart products should only be added if product is available", async () => {
  const { status: status1 } = await api.post("cart/chisel");
  expect(status1).toBe(200);
  const { status: status2 } = await api.post("cart/chisel");
  expect(status2).toBe(404);
  const { body: productBody } = await api.get("/product/chisel");
  expect(productBody.quantityAvailable).toBe(0);
});

test("Product totals should be tallied to give an overall total", async () => {
  await api.post("cart/chisel");
  await api.post("cart/hacksaw");
  const {
    body: { total }
  } = await api.get("cart/total");
  expect(total).toBe("33.35");
});

test("Must be able to remove a product from the cart", async () => {
  await api.post("cart/chisel");
  await api.post("cart/hacksaw");
  await api.del("cart/hacksaw");
  const { body: cart } = await api.get("cart");
  expect(cart.length).toBe(1);
});

test("Adding the same product twice will NOT create two cart items", async () => {
  await api.post("cart/chisel");
  await api.post("cart/chisel");
  const { body: cart } = await api.get("cart");
  expect(cart.length).toBe(1);
  expect(cart[0].quantity).toBe(1);
});

test("All prices should be rounded to 2 decimal places.", async () => {
  await api.post("cart/chisel");
  const { body: cart } = await api.get("cart");
  const [chisel] = cart;
  const { price } = chisel;
  const [_, cents] = price.split(".");
  expect(cents.length).toBe(2);
});

test("TODO: Prices should be calculated correctly (no float math)", async () => {
  // We obviously shouldn't be doing currency math with floats.
  // Can easily << 2 before then >> 2 after calculations but havent thought of a good test yet
  //  Out of time so just going to leave this
  await api.post("cart/chisel");
  await api.post("cart/bandsaw");
  const { body } = await api.get("cart/total");
  expect(1).toBe(2);
});
