const restify = require("restify");
const storage = require("node-persist");
const { productStorage, cartStorage } = require("./store");

const server = restify.createServer();

server.get("/product/:name", (req, res, next) => {
  return getProduct(req.params.name).then(product => {
    res.send(product);
    next();
  });
});

server.get("/products", (req, res, next) => {
  return productStorage.values().then(products => {
    res.send(products);
    next();
  });
});

async function getProduct(name) {
  return await productStorage.get(name);
}

async function getCartItem(name) {
  return await cartStorage.get(name);
}

server.post("/cart/:productname", async (req, res, next) => {
  const { productname: name } = req.params;
  const product = await getProduct(name);

  if (!product || !product.quantityAvailable) {
    res.send(404);
    return next();
  }

  const existingItemInCart = await getCartItem(name);

  return productStorage
    .set(
      name,
      Object.assign(product, {
        quantityAvailable: product.quantityAvailable - 1
      })
    )
    .then(() => {
      const quantity = existingItemInCart ? existingItemInCart.quantity + 1 : 1;
      return cartStorage
        .set(name, {
          name: product.name,
          price: toTwoDecimals(product.price),
          quantity,
          total: toTwoDecimals(quantity * product.price)
        })
        .then(() => {
          res.send(200);
          next();
        });
    });
});

server.get("/cart", (req, res, next) => {
  return cartStorage.values().then(cartItems => {
    res.send(cartItems);
    next();
  });
});

server.get("/cart/total", (req, res, next) => {
  return cartStorage.values().then(cartItems => {
    const total = cartItems.reduce((acc, item) => acc + +item.total, 0);
    res.send({ total: toTwoDecimals(total) });
    next();
  });
});

server.del("/cart/:productname", (req, res, next) => {
  return cartStorage.removeItem(req.params.productname).then(({ removed }) => {
    res.send(removed ? 200 : 404);
    next();
  });
});

server.listen(8080);

function toTwoDecimals(num) {
  return num.toFixed(2);
}
