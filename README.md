# Shopping Cart

All of the interesting info should be in index.test.js

To Run:

`npm i`
`npm run demo`

The server will now be running in the background:

Some example commands to check everything is working:-

`curl localhost:8080/cart`
`curl localhost:8080/product/chisel`
`curl -X POST localhost:8080/cart/chisel`
`curl localhost:8080/cart`
`curl localhost:8080/total`
