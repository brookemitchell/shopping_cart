const storage = require("node-persist");

const productStorage = storage.create({ dir: "products" });
const cartStorage = storage.create({ dir: "cart" });

const initialProducts = [
  {
    name: "Sledgehammer",
    price: 125.76,
    quantityAvailable: 2,
    addToCartLink: "cart/sledgehammer"
  },
  {
    name: "Bandsaw",
    price: 562.14,
    quantityAvailable: 3,
    addToCartLink: "cart/bandsaw"
  },
  {
    name: "Chisel",
    price: 13.9,
    quantityAvailable: 1,
    addToCartLink: "cart/chisel"
  },
  {
    name: "Hacksaw",
    price: 19.45,
    quantityAvailable: 1,
    addToCartLink: "cart/hacksaw"
  },
  {
    name: "MagicItem",
    price: 10009.45,
    quantityAvailable: 0,
    addToCartLink: "cart/magicitem"
  }
];

async function resetProducts() {
  await productStorage.clear();
  await loadInitialProducts();
}

async function loadInitialProducts() {
  const storeSetupPromises = initialProducts.map(entry =>
    productStorage.set(entry.name.toLowerCase(), entry)
  );
  return await Promise.all(storeSetupPromises);
}

// init storage here
(async () => {
  await productStorage.init();
  await cartStorage.init();
  await loadInitialProducts();
})();

module.exports = { productStorage, cartStorage, resetProducts };
